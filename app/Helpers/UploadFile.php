<?php

namespace App\Helpers;

use File;
use Carbon\Carbon;

class UploadFile{
  protected $filePath = 'files/';

  protected $file = null;
  protected $filename = null;

  public function __construct($file){
    $this->file = $file;
    $this->makeDir($this->filePath);
  }

  protected function randFilename(){
    $filename = Carbon::now()->timestamp.'_'.uniqid().'.'.$this->getExt();
    return $filename;
  }

  public function upload(){
    $filename = $this->getFilename();
    $this->makeDir($this->filePath);
    $file = $this->file;

    return $file->move($this->filePath, $filename);
  }

  protected function makeDir($path){
    if($path != ''){
      if(!File::isDirectory($path)){
        File::makeDirectory($path);
      }
    }
  }

  public function setPath($location){
    $this->filePath .= $location;
  }

  public function getPath(){
    return $this->filePath;
  }

  public function setFilename($filename){
    $this->filename = $filename.'.'.$this->getExt();
  }

  public function getFilename(){
    if(is_null($this->filename)){
      $this->filename = $this->randFilename();
    }
    return $this->filename;
  }

  public function getExt(){
    $file = $this->file;
    return strtolower($file->getClientOriginalExtension());
  }
}