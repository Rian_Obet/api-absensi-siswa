<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model{
    use SoftDeletes;

    protected $fillable = [
        'tier_id',
        'group_study_id',
        'subject_id',
        'image',
        'file'
    ];

    public function tier()
    {
        return $this->belongsTo('App\Models\Tier', 'tier_id');
    }

    public function group_study()
    {
        return $this->belongsTo('App\Models\GroupStudy', 'group_study_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject', 'subject_id');
    }
}
