<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alfa6661\AutoNumber\AutoNumberTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;

use App\Traits\Updater;

class Student extends Model implements AuthenticatableContract, AuthorizableContract{
    use SoftDeletes, Authenticatable, Authorizable, HasApiTokens;

    protected $fillable = [
        'school_id',
        'name',
        'nisn',
        'code',
        'gender',
        'tier_id',
        'group_study_id',
        'username',
        'password',
    ];

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => function () {
                    return 'S?' . date('Ymd');
                },
                'length' => 4
            ]
        ];
    }

    public function tier()
    {
        return $this->belongsTo('App\Models\Tier', 'tier_id');
    }

    public function group_study()
    {
        return $this->belongsTo('App\Models\GroupStudy');
    }

    public function absents()
    {
        return $this->HasMany('App\Models\Absent');
    }

    public function student_activity()
    {
        return $this->HasMany('App\Models\StudentActivity');
    }

    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }
}
