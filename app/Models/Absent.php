<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Absent extends Model{
    use SoftDeletes;
    
    protected $fillable = [
        'student_id',
        'date',
        'time_in',
        'time_out',
    ];

    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id');
    }

}
