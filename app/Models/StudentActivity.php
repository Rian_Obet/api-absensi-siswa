<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class StudentActivity extends Model{
    use SoftDeletes;

    protected $table = "student_activities";

    protected $fillable = [
        'student_id',
        'activity_id',
        'date',
        'image',
        'description',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class, 'activity_id');
    }
}
