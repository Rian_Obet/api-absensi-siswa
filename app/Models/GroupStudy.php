<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
USE Alfa6661\AutoNumber\AutoNumberTrait;

use App\Traits\Updater;

class GroupStudy extends Model{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
    ];

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => 'RB?',
                'length' => 2
            ]
            ];
    }

}
