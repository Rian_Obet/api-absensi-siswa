<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alfa6661\AutoNumber\AutoNumberTrait;

use App\Traits\Updater;

class Material extends Model{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'file',
        'image',
        'description',
        'subject_id',
        'school_id'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id', 'id');
    }

    public function tier()
    {
        return $this->belongsTo(Tier::class);
    }

    public function group_study()
    {
        return $this->belongsTo(GroupStudy::class);
    }

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => function () {
                    return 'MTR?' . date('Ymd');
                },
                'length' => 3
            ]
        ];
    }
}
