<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Activity extends Model
{
    use SoftDeletes;

    protected $table  = 'activities';

    protected $fillable = [
        'daily_activity_id',
        'date',
        'name',
    ];

    public function daily_activity()
    {
        return $this->belongsTo(DailyActivity::class, 'daily_activity_id');
    }

    public function student_activity()
    {
        return $this->hasMany(StudentActivity::class, 'student_activity_id');
    }
}
