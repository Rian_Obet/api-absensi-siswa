<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;
    
    protected $table = 'schedules';
    
    protected $guarded = [];

    public function tier_subject()
    {
        return $this->belongsTo(TierSubject::class);
    }

    public function group_study()
    {
        return $this->belongsTo(GroupStudy::class);
    }

    public function tier()
    {
        return $this->belongsTo(Tier::class);
    }
    
}
