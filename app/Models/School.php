<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alfa6661\AutoNumber\AutoNumberTrait;

use App\Traits\Updater;

class School extends Model{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
    ];

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => function () {
                    return 'SK?' . date('Ymd');
                },
                'length' => 3
            ]
        ];
    }
}
