<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alfa6661\AutoNumber\AutoNumberTrait;


use App\Traits\Updater;

class Tier extends Model{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'group_study_id',
    ];

    

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => 'T?',
                'length' => 2
            ]
            ];
    }
}
