<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'activity_id'   => $this->activity_id,
            'date'          => $this->date,
            'image'         => env('APP_BASE_URL') . '/files/studentactivities/' . $this->image,
            'descriptiom'   => $this->description,
            'activity'      => new ActivityResource($this->activity)
        ];
    }
}
