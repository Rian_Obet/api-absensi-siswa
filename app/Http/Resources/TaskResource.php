<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'tier_id'       => $this->tier_id,
            'group_study_id'=> $this->group_study_id,
            'subject_id'    => $this->subject_id,
            'image'         => env('APP_BASE_URL') . '/files/task/' . $this->image,
            'file'          => env('APP_BASE_URL') . '/files/task/' . $this->file,
            'tier'          => new TierResource($this->tier),
            'group_study'   => new GroupStudyResource($this->group_study),
            'subject'       => new SubjectResource($this->subject)
        ];
    }
}
