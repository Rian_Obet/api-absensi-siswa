<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'school_id' => $this->school_id,
            'name' => $this->name,
            'nisn' => $this->nisn,
            'code' => $this->code,
            'gender' => $this->gender,
            'tier_id' => $this->tier_id,
            'group_study_id' => $this->group_study_id,
            'username' => $this->username,
            // 'password' => $this->password,
            // 'token' => $this->token
        ];
    }
}
