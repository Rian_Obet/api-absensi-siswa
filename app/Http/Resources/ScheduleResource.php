<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'school_id'         => $this->school_id,
            'days'              => $this->days,
            'tier_id'           => $this->tier_id,
            'group_study_id'    => $this->group_study_id,
            'tier'              => new TierResource($this->tier),
            'group_study'       => new GroupStudyResource($this->group_study)
        ];
    }
}
