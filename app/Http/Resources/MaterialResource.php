<?php

namespace App\Http\Resources;
use App\Models\School;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'school_id'         => $this->school_id,
            'school'            => new SchoolResource($this->school),
            'code'              => $this->code,
            'file'              => env('APP_BASE_URL') . '/files/material/' . $this->file,
            'image'             => env('APP_BASE_URL') . '/files/material/' . $this->image,
            'description'       => $this->description,
            'subject_id'        => $this->subject_id,
            'subject'           => new SubjectResource($this->subject)
        ];
    }
}
