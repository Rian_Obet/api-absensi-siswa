<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'school_id' => $this->school_id,
            'name' => $this->name,
            'nisn' => $this->nisn,
            'code' => $this->code,
            'gender' => $this->gender,
            'tier_id' => $this->tier_id,
            'group_study_id' => $this->group_study_id,
            'username' => $this->username,
            'token' => $this->token
        ];
    }
}
