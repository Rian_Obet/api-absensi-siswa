<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'daily_activity_id'  => $this->daily_activity_id,
            'date' => $this->date,
            'name' => $this->name,
            'daily_activity' => new DailyActivityResource($this->daily_activity)
        ];
    }
}
