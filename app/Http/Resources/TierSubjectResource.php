<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TierSubjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'schedule_id'    => $this->schedule_id,
            'subject_id'     => $this->subject_id,
            'schedule'       => new ScheduleResource($this->schedule),
            'subject'        => new SubjectResource($this->subject),
        ];
    }
}
