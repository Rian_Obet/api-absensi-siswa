<?php

namespace App\Http\Controllers;

use App\Http\Resources\AbsentResource;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Absent;
use Carbon\Carbon;

class AbsentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Absent $model)
    {
        $this->model        = $model;
        // $this->student_id   = Auth::guard('student')->id;
    }


    public function index()
    {
        try {
            $data = $this->model->where('date', Carbon::today())->first();

            if ($data) {
                $messages = [
                    'code' => 200,
                    'message' => 'successfully',
                    'data' => new AbsentResource($data)
                ];
            }else{
                $messages = [
                    'code' => 200,
                    'message' => 'Data Kosong !',
                    'data' => null
                ];
            }

        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }
        
        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }

    public function store(Request $request)
    {
        try {
            $input["student_id"] = Auth::guard('student')->user()->id;

            $input['date'] = date('Y-m-d');

            $input['time_in'] = date('H:i:s');

            $data = $this->model->create($input);

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new AbsentResource($data) 
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }    

    public function update(Request $request, $id)
    {
        try {
            $data = $this->model->findOrFail($id);
            $data->time_out = date('H:i:s');
            $data->save();

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new AbsentResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }
}
