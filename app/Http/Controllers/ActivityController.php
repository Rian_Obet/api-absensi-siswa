<?php

namespace App\Http\Controllers;

use App\Http\Resources\StudentActivityResource;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Helpers\UploadImg;
use App\Http\Resources\ActivityResource;
use App\Models\Activity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\StudentActivity;

class ActivityController extends Controller
{

    public function index()
    {
        $datas = Activity::whereDate('date', '=', date('Y-m-d'))->paginate(10);

        return ActivityResource::collection($datas);
    }
}
