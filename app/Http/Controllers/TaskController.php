<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFondation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Helpers\UploadImg;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Task;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Task $model)
    {
        $this->model    = $model;
    }


    public function store(Request $request)
    {
        try {
            $input = $request->all();
            
            $picture = $request->file('image');
            if ($picture) {
                $img = Image::make($picture->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('png');

                $directoryName = "task";
                $pictureName = Carbon::now()->timestamp.'_'.uniqid().'.'.strtolower($picture->getClientOriginalExtension());
                $path = $directoryName . '/' . $pictureName;
                $img_resources = $img->getEncoded();
                Storage::disk('public')->put($path, $img_resources, 'public');

                $input['image'] = $pictureName;
            }

            $file = $request->file('file');
            if ($file){
                $directoryName = "task";
                $fileName = Carbon::now()->timestamp.'_'.uniqid().'.'.strtolower($file->getClientOriginalExtension());
                $path = $directoryName . '/' . $fileName;
                $img_resources = $img->getEncoded();
                Storage::disk('public')->put($path, $img_resources, 'public');
                
                $input['file'] = $fileName;
            }

            $data = $this->model->create($input);

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new TaskResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }    

}
