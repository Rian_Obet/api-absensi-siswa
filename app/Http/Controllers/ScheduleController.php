<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Helpers\UploadImg;
use App\Http\Resources\TierSubjectResource;
use App\Models\TierSubject;
use App\Models\Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\StudentActivity;

class ScheduleController extends Controller
{

    public function index()
    {   
        $school_id = Auth::guard('student')->user()->school_id;

        $tier_id = Auth::guard('student')->user()->tier_id;
        
        $group_study_id = Auth::guard('student')->user()->group_study_id;
        
        $schedule_id = Schedule::where('school_id',$school_id)->where('tier_id',$tier_id)->where('group_study_id',$group_study_id)->select('id')->first();
        // dd($schedule_id->id);
        $datas = TierSubject::where('schedule_id', $schedule_id->id)->get();
        // dd($datas);
        return TierSubjectResource::collection($datas);
    }
}
