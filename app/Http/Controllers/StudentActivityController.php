<?php

namespace App\Http\Controllers;

use App\Http\Resources\StudentActivityResource;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Helpers\UploadImg;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\StudentActivity;

class StudentActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StudentActivity $model)
    {
        $this->model    = $model;
    }

    public function getAll()
    {
        $datas = StudentActivity::all();

        $messages = [
            'code' => 200,
            'message' => 'successfully',
            'data' => StudentActivityResource::collection($datas)
        ];

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }


    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $input["student_id"] = Auth::guard('student')->user()->id;
            $input['date'] = date('Y-m-d');
            $input['description'] = 'Hadir';

            $picture = $request->file('image');
            if ($picture) {
                $img = Image::make($picture->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('png');

                $directoryName = "student-activities";
                $pictureName = Carbon::now()->timestamp . '_' . uniqid() . '.' . strtolower($picture->getClientOriginalExtension());
                $path = $directoryName . '/' . $pictureName;
                $img_resources = $img->getEncoded();
                Storage::disk('public')->put($path, $img_resources, 'public');

                $input['image'] = $pictureName;
            }

            $data = $this->model->create($input);

            $messages = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new StudentActivityResource($data)
            ];
        } catch (\Exception $e) {
            $messages = [
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }
}
