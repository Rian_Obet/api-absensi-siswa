<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Resources\LoginResource;
use App\Http\Resources\StudentResource;
use App\Http\Controllers\Controller;

use App\Models\Student;

use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct(
        Student $model
    ) {
        $this->model = $model;
    }
     
        public function loginStudent(Request $request)
        {
            try {
                $data =$this->model
                ->where('username', $request['username'])
                ->first();
                if ($data) {
                    if (Hash::check($request['password'], $data->password)) {
                        
                        $data['token'] = $data->createToken($data->id)->accessToken;
    
                        $messages = [
                            'code' => 200,
                            'message' => "successfully",
                            'data' => new LoginResource($data)
                        ];
                    } else {
                        $messages = [
                            'code' => 400,
                            'message' => "Email dan password Anda salah!"
                        ];
                    }
                } else {
                    $messages = [
                        'code' => 400,
                        'message' => "Email dan password Anda salah!"
                    ];
                }
            } catch (\Exception $e) {
                $messages = [
                    'code' => 500,
                    'message' => $e->getMessage()
                ];
            }
    
            return response()
                ->json($messages, $messages['code'])
                ->header('Content-Type', 'application/json');
        }
     
}
