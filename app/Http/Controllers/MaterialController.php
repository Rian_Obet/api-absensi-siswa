<?php

namespace App\Http\Controllers;

use App\Http\Resources\AbsentResource;
use App\Http\Resources\MaterialResource;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Absent;
use App\Models\Material;
use App\Models\Student;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class MaterialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Material $model, Student $model_student)
    {
        $this->model            = $model;
        $this->model_student    = $model_student;
        // $this->school_id        = Auth::guard('student')->student()->school()->id; 
    }

    public function getAll()
    {
        $datas = Material::paginate(10);

        return MaterialResource::collection($datas);
    }

    public function getFile($id)
    {
        $data = Material::findOrFail($id);

        $messages = [
            'code' => 200,
            'message' => 'successfully',
            'data' => [
                'link' => env('APP_BASE_URL') . '/files/material/'.$data->file
            ]
        ];

        return response()
            ->json($messages, $messages['code'])
            ->header('Content-Type', 'application/json');
    }    
}
