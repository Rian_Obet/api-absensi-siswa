<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function () {
    return \Illuminate\Support\Str::random(32);
});


// Login
$router->post("/login", "LoginController@loginStudent");

$router->group(['middleware' => ['auth:student']], function () use ($router) {

    //Absent in
    $router->post("absent", 'AbsentController@store');

    $router->get("absent", 'AbsentController@index');

    //absent out
    $router->post("absent/{id}", 'AbsentController@update');

    // Material
    $router->get("material", "MaterialController@getAll");

    $router->get("material/getFile/{id}", 'MaterialController@getFile');

    //absent activity
    $router->get("all/activity", "StudentActivityController@getAll");

    $router->post("activity", 'StudentActivityController@store');

    $router->get('activity', 'ActivityController@index');

    $router->get('schedule', 'ScheduleController@index');

    //upload task
    $router->post("task", 'TaskController@store');
});
